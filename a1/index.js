let getCube = (number) => {
	let cube = number ** 3
	console.log(`The cube of ${number} is ${cube}.`)
};

getCube(2);

const fullAddress = ["742 Evergreen Terrace",
	"Springfield", "USA"]


const [street, city, country] = fullAddress;

console.log(`I live in ${street}, ${city}, ${country}.`);

let animal = {
	name : "Reggie",
	species : "Koala",
	weight : 12,
	height : 67
};

const {name,species,weight,height} = animal;
console.log(`${name} is a ${species}. He weighed at ${weight}kg with a measurement of ${height}cm.`);

let numbers = [1,2,3,4,5];

numbers.forEach((number)=>{
	console.log(number);
})

let reducedNumber = numbers.reduce((prev,current) => prev + current)

console.log(reducedNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

const myDog = new Dog ("Yumi", 1, "Bichon Frise");

console.log(myDog);