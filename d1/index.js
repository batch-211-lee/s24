console.log("Hello World");

//ES6 updates

//ES6 is one of the latest versions of writing JS and in fact is one of the major update
// let,const - are ES6 updates, these are the new standards of creating variables

//Exponent Operator

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);

let string1 = "fun";
let string2 = "Bootcamp";
let string3 = "Coding";
let string4 = "Javascript";
let string5 = "Zuitt";
let string6 = "Learning";
let string7 = "love";
let string8 = "I";
let string9 = "is";
let string10 = "in";

// let sentence1 = string8 +  " " + string7 + " " + string6 + " " + string4 + "!"
// let sentence2 = string6 + " " + string10 + " " +string5 + " " + string2 + " " + string9 + " " + string1 + ".";
// console.log(sentence1);
// console.log(sentence2);

//Template literals
	//Template Literals allows us to create strings using `` and easily embed JS expression in it
		/*
			allows us to write string without using the concatentation operator (+) and greatly helps with code readability
		*/
// "", '' - string literals

let sentence1 = `${string8} ${string7} ${string5} ${string3} ${string2}!`;
console.log(sentence1); 

/*
	${} is a placeholder that is used to embed JS expressions when creating strings using Template Literals
*/

//Pre-Template Literal String
// ("") or ('')

let name = "John";

let message = "Hello" + name + "! Welcome to programming";

//Strings using template literals
// (``)

message = `Hello ${name}! Welcome to programming.`;

//Multi-line using template literals

const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.

`;
console.log(anotherMessage);
console.log(message);

let dev = {
	name : "Peter",
	lastName : "Parker",
	occupation : "web developer",
	income : 50000,
	expense : 60000
};

console.log(`${dev.name} is a ${dev.occupation}.`);
console.log(`His income is ${dev.income} and expenses at ${dev.expense}. his current balance is ${dev.income - dev.expense}.`);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on you saving account is ${principal * interestRate}.`);

//Array Destructuring

/*
	Allows us to unpack elements in arrays into distinct variables

	Syntax:
		let/const [variableNameA, variableNameB, variableNameC] = array;
*/

const fullName = ["Juan", "Dela", "Cruz", "Y.", "Rizal"];

//Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you.`);

//Array Destructing

const [firstName, middleName, lastName,,anotherName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(anotherName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It is nice to meet you.`);

//Object Destructuring
/*
	Allows us to unpack properties of objects into distinct variables

	Syntax:
	let/const {propertyNameA, propertyNameB, propertyNameC} = object
*/

const person = {
	givenName : "Jane",
	maidenName : "Dela",
	familyName : "Cruz"
};

//Pre-Object Destructuring
//We can access them using . or []

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

//Object Destructuring

const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

//Arrow Functions
/*
	Compact alternative syntax to traditional functions
*/

const hello = () => {
	console.log("Hello World")
};

hello();//alternative to the function below

// const hello = function hello(){
// 	console.log("Hello World")
// };

// hello();

//Pre-Arrow Function and Template Literals
/*
	Syntax:
	function functionName(parameterA){
		console.log();
	}
*/

//Arrow Function
/*
	Syntax:
	let/const variableName = (parameterA) =>{
		console.log();
	};
*/

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
};

printFullName("John", "D", "Smith");

//Arrow Functions with Loops

//Pre-arrow function

const students = ["Jonh", "Jane", "Judy"]

students.forEach(function(student){
	console.log(`${student} is a student.`)
});

//Arrow Function

students.forEach((student)=>{
	console.log(`${student} is a student.`)
});

//Implicit Return Statement
/*
		There are instance when you can omit the "return" statement
		This works because even without the "return" statement JS IMPLICITLY adds it for the result of the function
*/

//Pre-Arrow function

// const add = (x,y) => {
// 	return x + y;
// };

const add = (x,y) => x + y

let total = add(1,2);
console.log(total);

	// {} in an arrow function are code blocks. if an arrow function has a {} or code block we're going to need a return

	//implicit return will only work on arrow function without {}

const sub = (x,y) => x - y;
const multi = (x,y) => x * y;
const div = (x,y) => x / y;

total = sub(1,2);
console.log(total);
total = multi(1,2);
console.log(total);
total = div(1,2);
console.log(total);

//Default Function Argument Value
//provide a default argument value if none is provided when the function is invoked

const greet = (name = "User") => {
	return `Good evening, ${name}`
};

console.log(greet());
console.log(greet("John"));

//Class-Based Object Blueprint
/*
	Allows creation/instantiation of objects using classes as blueprint
*/

//Creating a class
/*
	the constructor is a special method of a class for creating/initializing and object for that class

	Syntax:
		class className {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

class Car {
	constructor(brand,name,year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};

//Instantiate an object
/*
	the "new" operator creates/instantiate a new object with the given arguments as the value of its properties

	Syntax:
		let/const variablename = new className();
*/

/*
	Creating a constant with the const keyword and assigning it a value of an object nmakes it so we can't reassign it with another data type.
*/

const myCar = new Car ();

console.log(myCar);//undefined

myCar.brand = "Ford";
myCar.name = "Everest";
myCar.year = 1996;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);

class Fighter{
	constructor(name,role, strength,weakness){
		this.name = name;
		this.role = role;
		this.strength = strength;
		this.weakness = weakness;
	}
};

const myFighter = new Fighter ("Cammy", "Rushdown", "Up-Close", "Grapplers");
const myNewFighter = new Fighter ("King", "Zoner", "Distance", "Rushdowns");
console.log(myFighter);
console.log(myNewFighter);